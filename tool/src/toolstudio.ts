/**
 * ObjectPtr is a C family-style double pointer to an object to enable updating
 * dictionaries and lists.
 */
class ObjectPtr {
	obj: object;
	key: string;

	constructor(obj: object, key: string) {
		this.obj = obj;
		this.key = key;
	}

	/**
	 * pointTo changes the value of the pointer so the getters and setters update the value associated
	 * with the specified key in the specified object.
	 * @param {object} obj must be a non-null defined object.
	 * @param {string} key key for the ObjectPtr to manage.
	 */
	pointTo (obj: object, key: string) {
		this.obj = obj;
		this.key = key;
	}

	get value() { return this.obj[this.key]; }
	set value(val) { this.obj[this.key] = val; }
}

/**
 * EventObject is an event descriptor used by the EventEmitter.
 */
class EventObject {
	/**
	 *Additional data for this event
	 */
	data: any;

	/**
	 * The sender of the event. This can be any object.
	 */
	source: object;

	/**
	 * Name of the event. This can be used to differentiate between different event types.
	 */
	name: string;

	stop: boolean;

	constructor(name: string, source: object, data: any) {
		this.name = name;
		this.source = source;
		this.data = data;
		this.stop = false;
	}

	preventDefault() {
		this.stop = true;
	}
}

type EventHandler = (event: EventObject) => void;

/**
 * Simple event dispatcher(emitter) to implement a pub/sub.
 */
class EventEmitter {
	eventTable: Map<string, EventHandler>;

	constructor() {
		// map event to a list of a consumers.
		this.eventTable = new Map<string, EventHandler>(); 
	}

	/**
	 * Dispatch an event with its sender and parameters.
	 * @param {string} name - The name of the event.
	 * @param {object} source - The sender of the event.
	 * @param {object} data - Optional event args for this event.
	 */
	dispatch(event: EventObject) {
		if (!this.eventTable.hasOwnProperty(event.name)) return;
		this.eventTable[event.name].forEach((handler: EventHandler) => {
			if (event.stop) {
				return;
			}
			try {
				handler(event);
			} catch (e) {
				console.error(e);
			}
		});
	}

	/**
	 * Subscribe to an event.
	 * @param {string} name - the name of the event to subscribe to.
	 * @param {function} handler - an event handler method.
	 */
	addConsumer(name: string, handler: EventHandler) {
		if (!this.eventTable.hasOwnProperty(name)) this.eventTable[name] = [];
		this.eventTable[name].splice(0, 0, handler);
	}
}

/**
 * Returns a reference to the first object with a dpath set to the name specified.
 * @param {string} dpathName - the value of the dpath attribute.
 */
function getElementByPath(dpath: string) {
	return this.querySelector(`[dpath='${dpath}']`);
};

/**
 * RequestData describes a tool evaluation request data.
 */
interface RequestData {
	body: any;
	contentType: string;
};

/**
 * RequestHandler describes the ability to submit an evaluation request.
 */
type RequestHandler = (request: Request) => void;

interface FormDataCollector {
	collect(form: HTMLFormElement): RequestData;
};

class JsonFormDataCollector {
	ignoreEmptyValues: boolean;

	collect(form: HTMLFormElement): RequestData {
		const root = (form instanceof Element ? form : document.documentElement);
		const data = {};
		this.scan(root, data);
		return {
			body: data,
			contentType: 'application/json',
		};
	}

	/**
	 * Looks through all DOM elements and fill the provided currentObj with collected values. This method gets access to the root data
	 * in case absolute key paths are used. currentObj needs to be the object inside the root object where the relative
	 * key paths are going to be starting from.
	 * @example
	 * let root = {
	 *   'user': {}
	 * };
	 * let currentObj = root.user;
	 * // In the example above, keypath "info" would be relative to currentObj so user.info in root would get set.
	 * // However, ".info" would be an absolute keypath and it would set "info" in the root object.
	 * @param {DOM} element - The root dom element to start the search from. Use document to search from the top.
	 * @param {object} root - The root object.
	 * @param {object} currentObj - The current object that will used for relative key paths. This MUST exist in the
	 * root object. When starting from root, set currentObj to null.
	 * @param {Set} visitedNodes - The set of already visited nodes. These nodes will get ignored.
	 */
	scan(element: Element, root: object, currentObj?: object, visitedNodes?: Set<Element>) {
		let query = element.querySelectorAll('[dpath]');
		currentObj = currentObj || root;
		visitedNodes = visitedNodes || new Set<Element>();
		query.forEach((element) => {
			if (visitedNodes.has(element)) return;
			visitedNodes.add(element);
			let path = element.getAttribute('dpath');
			let value: any;
			if (element instanceof HTMLInputElement) {
				value = element.value;
			} else {
				value = {};
				this.scan(element, root, value, visitedNodes);
			}
			if (element.hasAttribute('dmap')) {
				// TODO: find an alternative method to this unsafe eval.
				value = function(code: string) { return eval(code); }.call(value, element.getAttribute('dmap'));
			}
			let action = null;
			if (!element.hasAttribute('daction')) action = 'set';
			else {
				let dactionValue = element.getAttribute('daction');
				if (dactionValue === "set" || dactionValue === "append") action = dactionValue;
				else{
					let actionContext = {value: value, element: element};
					action = function (code: string) { return eval(code); }.call(actionContext, element.getAttribute('daction'));
					if (!action) return;  // if the evaluated daction equals a false value, ignore the data altogether.
				}
			}
			if (this.ignoreEmptyValues) {
				if (value instanceof Object && Object.keys(value).length < 1) return;
				if (value instanceof Array && value.length < 1) return;
				if ((value instanceof String || typeof(value) === "string") && value.length < 1) return;
			}
			let targetObj = currentObj;
			if (path[0] === '.') {
				targetObj = root;
				path = path.substr(1);
			}
			this.updateValue(targetObj, path, value, action);
		});
	}

	/**
	 * Sets the value for the given key path in the provided object.
	 * @param {object} obj - The target object, where the new value is going to live in.
	 * @param {string} key - The key path for the object. e.x. "user.info.name"
	 * @param {any} value - The value to be set.
	 * @param {string} action - The action to use for inserting this new object. "set" will replace any existing item.
	 * "append" will use an array and append the provided value.
	 */
	updateValue(obj: object, path: string, value: any, action: string) {
		let parts = path.split('.');
		let cursor = new ObjectPtr(obj, parts.shift());
		parts.forEach((part) => {
			if (typeof(part) === 'number' && !(cursor.value instanceof Array))
				cursor.value = [];
			else if (!(cursor.value instanceof Object))
				cursor.value = {};
			cursor.pointTo(cursor.value, part);
		});
		if (action === 'set')
			cursor.value = value;
		else if (action === 'append') {
			if (cursor.value instanceof Array)
				cursor.value.push(value);
			else
				cursor.value = [value];
		}
	}

	resolve(obj: object, parts: string[]) { return parts.reduce((o, i) => (o && o.hasOwnProperty(i) ? o[i] : null), obj); }
}
const defaultJsonCollector = new JsonFormDataCollector();

interface Element { getElementByPath(dpath: string): Element; }
interface Document { getElementByPath(dpath: string): Element; }
Element.prototype.getElementByPath = getElementByPath;
Document.prototype.getElementByPath = getElementByPath;

const defaultEmitter = new EventEmitter();
const DefaultManager = {
	/**
	 * autoInstallOnForms overrides all available Form elements' submit handler.
	 */
	autoInstallOnForms: true,

	collectors: {
		'application/json': defaultJsonCollector,
		'json': defaultJsonCollector,
	},

	/**
	 * url holds the HTTP/gRPC endpoint to use to reach the backend.
	 * it gets loaded after contacting the iframe parent.
	 */
	url: "",

	/**
	 * fetch is a wrapper around the standard fetch function but it uses the appropriate URL
	 * and sets the method to POST. This is just a shortcut method.
	 */
	fetch(options?: RequestInit): Promise<Response> {
		if (options === undefined)
			options = {};
		options.method = 'POST';
		return fetch(this.url, options);
	},

	/**
	 * submitHandler is the method that gets used by the toolstudio for submitting collected data.
	 */
	submitHandler: function(request: RequestData) {
		const options: RequestInit = {body: request.body};
		if (request.contentType)
			options.headers = {'Content-Type': request.contentType};
		this.fetch(options).then((response: Response) => {
			defaultEmitter.dispatch(new EventObject('fetch_response', this, response));
		}).catch((error: any) => {
			defaultEmitter.dispatch(new EventObject('fetch_error', this, error));
		});
	},

	/**
	 * collectedData attempts to produce appropriate data from a form.
	 * enctype of application/json activates specific toolstudio data collection from DOM elements
	 * in the form.
	 */
	collectFormData(form: HTMLFormElement): RequestData {
		const enctype = form.getAttribute('enctype');
		const collector = this.collectors[enctype];
		if (collector)
			return collector.collect(form);
		else
			return {body: new FormData(form), contentType: enctype};
	},

	handleFormSubmit(event: Event): boolean {
		try {
			const input: RequestData = this.collectFormData(event.target);
			const dataEvent = new EventObject('data_ready', this, input);
			defaultEmitter.dispatch(dataEvent);
			if (dataEvent.stop === false && this.submitHandler)
				this.submitHandler(input);
		} catch (e) {
			console.error(e);
		}

		event.preventDefault();
		return false;
	},

	/**
	 * installOnForm overrides the default form handler 
	 */
	installOnForm(form: HTMLFormElement) {
		form.onsubmit = this.handleFormSubmit.bind(this);
		form.method = 'POST';
	},

	/**
	 * Scan the DOM elements and replace the action with the toolstudio form handler.
	 * Ignoring any form that has 'dignore="1"' on it.
	 */
	installOnForms() {
		for (let index = 0, len = document.forms.length; index < len; index++) {
			const form = document.forms[index];
			if (form.getAttribute("dignore") == "1") continue;
			this.installOnForm(form);
		}
	},

	/**
	 * on adds a new event handler to the specific event channel.
	 */
	on(name: string, handler: EventHandler) {
		defaultEmitter.addConsumer(name, handler);
	}

};

window.addEventListener('DOMContentLoaded', () => {
	if (DefaultManager.autoInstallOnForms) {
		DefaultManager.installOnForms();
	}
});

window.addEventListener('message', (event: MessageEvent) => {
	DefaultManager.url = event.data.http;
});

global.toolstudio = DefaultManager;
