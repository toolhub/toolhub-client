var path = require('path');

module.exports = {
	mode: "production",
	entry: "./src/toolstudio.ts",
	resolve: {
		extensions: ['.ts', '.js'],
	},
	module: {
		rules: [
			{test: /\.ts$/, loader: 'ts-loader'}
		],
	},
	output: {
		filename: 'toolstudio.min.js',
		path: path.resolve(__dirname, 'dist'),
	},
};
