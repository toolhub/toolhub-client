import * as jspb from 'google-protobuf'



export class Status extends jspb.Message {
  getStatus(): Status.Overall;
  setStatus(value: Status.Overall): Status;

  getBackendStatus(): Status.BackendStatus;
  setBackendStatus(value: Status.BackendStatus): Status;

  getFrontendStatus(): Status.FrontendStatus;
  setFrontendStatus(value: Status.FrontendStatus): Status;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Status.AsObject;
  static toObject(includeInstance: boolean, msg: Status): Status.AsObject;
  static serializeBinaryToWriter(message: Status, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Status;
  static deserializeBinaryFromReader(message: Status, reader: jspb.BinaryReader): Status;
}

export namespace Status {
  export type AsObject = {
    status: Status.Overall,
    backendStatus: Status.BackendStatus,
    frontendStatus: Status.FrontendStatus,
  }

  export enum Overall { 
    ERROR = 0,
    PARTIAL = 1,
    WAITING = 2,
    READY = 3,
  }

  export enum BackendStatus { 
    BACKEND_ERROR = 0,
    BACKEND_INSTALLING = 1,
    BACKEND_READY = 2,
  }

  export enum FrontendStatus { 
    FRONTEND_MISSING = 0,
    FRONTEND_CLEAN_UP_QUEUE = 1,
    FRONTEND_CLEANING_UP = 2,
    FRONTEND_INSTALLATION_QUEUE = 3,
    FRONTEND_INSTALLING = 4,
    FRONTEND_READY = 5,
    FRONTEND_ERROR = 6,
  }
}

export class Account extends jspb.Message {
  getId(): string;
  setId(value: string): Account;

  getName(): string;
  setName(value: string): Account;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Account.AsObject;
  static toObject(includeInstance: boolean, msg: Account): Account.AsObject;
  static serializeBinaryToWriter(message: Account, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Account;
  static deserializeBinaryFromReader(message: Account, reader: jspb.BinaryReader): Account;
}

export namespace Account {
  export type AsObject = {
    id: string,
    name: string,
  }
}

export class Metadata extends jspb.Message {
  getTagsList(): Array<string>;
  setTagsList(value: Array<string>): Metadata;
  clearTagsList(): Metadata;
  addTags(value: string, index?: number): Metadata;

  getDescription(): string;
  setDescription(value: string): Metadata;

  getVersion(): string;
  setVersion(value: string): Metadata;

  getSourceUrl(): string;
  setSourceUrl(value: string): Metadata;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Metadata.AsObject;
  static toObject(includeInstance: boolean, msg: Metadata): Metadata.AsObject;
  static serializeBinaryToWriter(message: Metadata, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Metadata;
  static deserializeBinaryFromReader(message: Metadata, reader: jspb.BinaryReader): Metadata;
}

export namespace Metadata {
  export type AsObject = {
    tagsList: Array<string>,
    description: string,
    version: string,
    sourceUrl: string,
  }
}

export class Function extends jspb.Message {
  getName(): string;
  setName(value: string): Function;

  getGroup(): string;
  setGroup(value: string): Function;

  getStatus(): Status | undefined;
  setStatus(value?: Status): Function;
  hasStatus(): boolean;
  clearStatus(): Function;

  getOwner(): Account | undefined;
  setOwner(value?: Account): Function;
  hasOwner(): boolean;
  clearOwner(): Function;

  getMetadata(): Metadata | undefined;
  setMetadata(value?: Metadata): Function;
  hasMetadata(): boolean;
  clearMetadata(): Function;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Function.AsObject;
  static toObject(includeInstance: boolean, msg: Function): Function.AsObject;
  static serializeBinaryToWriter(message: Function, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Function;
  static deserializeBinaryFromReader(message: Function, reader: jspb.BinaryReader): Function;
}

export namespace Function {
  export type AsObject = {
    name: string,
    group: string,
    status?: Status.AsObject,
    owner?: Account.AsObject,
    metadata?: Metadata.AsObject,
  }
}

export class ListFunctionRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFunctionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFunctionRequest): ListFunctionRequest.AsObject;
  static serializeBinaryToWriter(message: ListFunctionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFunctionRequest;
  static deserializeBinaryFromReader(message: ListFunctionRequest, reader: jspb.BinaryReader): ListFunctionRequest;
}

export namespace ListFunctionRequest {
  export type AsObject = {
  }
}

export class ListFunctionResponse extends jspb.Message {
  getFunctionsList(): Array<Function>;
  setFunctionsList(value: Array<Function>): ListFunctionResponse;
  clearFunctionsList(): ListFunctionResponse;
  addFunctions(value?: Function, index?: number): Function;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFunctionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListFunctionResponse): ListFunctionResponse.AsObject;
  static serializeBinaryToWriter(message: ListFunctionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFunctionResponse;
  static deserializeBinaryFromReader(message: ListFunctionResponse, reader: jspb.BinaryReader): ListFunctionResponse;
}

export namespace ListFunctionResponse {
  export type AsObject = {
    functionsList: Array<Function.AsObject>,
  }
}

export class GetFunctionRequest extends jspb.Message {
  getName(): string;
  setName(value: string): GetFunctionRequest;

  getGroup(): string;
  setGroup(value: string): GetFunctionRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFunctionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFunctionRequest): GetFunctionRequest.AsObject;
  static serializeBinaryToWriter(message: GetFunctionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFunctionRequest;
  static deserializeBinaryFromReader(message: GetFunctionRequest, reader: jspb.BinaryReader): GetFunctionRequest;
}

export namespace GetFunctionRequest {
  export type AsObject = {
    name: string,
    group: string,
  }
}

export class GetFunctionResponse extends jspb.Message {
  getFunction(): Function | undefined;
  setFunction(value?: Function): GetFunctionResponse;
  hasFunction(): boolean;
  clearFunction(): GetFunctionResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFunctionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFunctionResponse): GetFunctionResponse.AsObject;
  static serializeBinaryToWriter(message: GetFunctionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFunctionResponse;
  static deserializeBinaryFromReader(message: GetFunctionResponse, reader: jspb.BinaryReader): GetFunctionResponse;
}

export namespace GetFunctionResponse {
  export type AsObject = {
    pb_function?: Function.AsObject,
  }
}

export class RPCEndpoint extends jspb.Message {
  getGrpc(): string;
  setGrpc(value: string): RPCEndpoint;

  getGateway(): string;
  setGateway(value: string): RPCEndpoint;

  getHttp(): string;
  setHttp(value: string): RPCEndpoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RPCEndpoint.AsObject;
  static toObject(includeInstance: boolean, msg: RPCEndpoint): RPCEndpoint.AsObject;
  static serializeBinaryToWriter(message: RPCEndpoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RPCEndpoint;
  static deserializeBinaryFromReader(message: RPCEndpoint, reader: jspb.BinaryReader): RPCEndpoint;
}

export namespace RPCEndpoint {
  export type AsObject = {
    grpc: string,
    gateway: string,
    http: string,
  }
}

export class AccessEndpoint extends jspb.Message {
  getInsecure(): boolean;
  setInsecure(value: boolean): AccessEndpoint;

  getApi(): RPCEndpoint | undefined;
  setApi(value?: RPCEndpoint): AccessEndpoint;
  hasApi(): boolean;
  clearApi(): AccessEndpoint;

  getDispatcher(): RPCEndpoint | undefined;
  setDispatcher(value?: RPCEndpoint): AccessEndpoint;
  hasDispatcher(): boolean;
  clearDispatcher(): AccessEndpoint;

  getCds(): string;
  setCds(value: string): AccessEndpoint;

  getDashboard(): string;
  setDashboard(value: string): AccessEndpoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AccessEndpoint.AsObject;
  static toObject(includeInstance: boolean, msg: AccessEndpoint): AccessEndpoint.AsObject;
  static serializeBinaryToWriter(message: AccessEndpoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AccessEndpoint;
  static deserializeBinaryFromReader(message: AccessEndpoint, reader: jspb.BinaryReader): AccessEndpoint;
}

export namespace AccessEndpoint {
  export type AsObject = {
    insecure: boolean,
    api?: RPCEndpoint.AsObject,
    dispatcher?: RPCEndpoint.AsObject,
    cds: string,
    dashboard: string,
  }
}

export class ListEndpointRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListEndpointRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListEndpointRequest): ListEndpointRequest.AsObject;
  static serializeBinaryToWriter(message: ListEndpointRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListEndpointRequest;
  static deserializeBinaryFromReader(message: ListEndpointRequest, reader: jspb.BinaryReader): ListEndpointRequest;
}

export namespace ListEndpointRequest {
  export type AsObject = {
  }
}

export class ListEndpointResponse extends jspb.Message {
  getEndpointsMap(): jspb.Map<string, AccessEndpoint>;
  clearEndpointsMap(): ListEndpointResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListEndpointResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListEndpointResponse): ListEndpointResponse.AsObject;
  static serializeBinaryToWriter(message: ListEndpointResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListEndpointResponse;
  static deserializeBinaryFromReader(message: ListEndpointResponse, reader: jspb.BinaryReader): ListEndpointResponse;
}

export namespace ListEndpointResponse {
  export type AsObject = {
    endpointsMap: Array<[string, AccessEndpoint.AsObject]>,
  }
}

export class BackendSpec extends jspb.Message {
  getImage(): string;
  setImage(value: string): BackendSpec;

  getType(): string;
  setType(value: string): BackendSpec;

  getPort(): number;
  setPort(value: number): BackendSpec;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BackendSpec.AsObject;
  static toObject(includeInstance: boolean, msg: BackendSpec): BackendSpec.AsObject;
  static serializeBinaryToWriter(message: BackendSpec, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BackendSpec;
  static deserializeBinaryFromReader(message: BackendSpec, reader: jspb.BinaryReader): BackendSpec;
}

export namespace BackendSpec {
  export type AsObject = {
    image: string,
    type: string,
    port: number,
  }
}

export class FrontendSpec extends jspb.Message {
  getSource(): string;
  setSource(value: string): FrontendSpec;

  getType(): string;
  setType(value: string): FrontendSpec;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FrontendSpec.AsObject;
  static toObject(includeInstance: boolean, msg: FrontendSpec): FrontendSpec.AsObject;
  static serializeBinaryToWriter(message: FrontendSpec, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FrontendSpec;
  static deserializeBinaryFromReader(message: FrontendSpec, reader: jspb.BinaryReader): FrontendSpec;
}

export namespace FrontendSpec {
  export type AsObject = {
    source: string,
    type: string,
  }
}

export class CreateFunctionRequest extends jspb.Message {
  getName(): string;
  setName(value: string): CreateFunctionRequest;

  getGroup(): string;
  setGroup(value: string): CreateFunctionRequest;

  getOwner(): Account | undefined;
  setOwner(value?: Account): CreateFunctionRequest;
  hasOwner(): boolean;
  clearOwner(): CreateFunctionRequest;

  getMetadata(): Metadata | undefined;
  setMetadata(value?: Metadata): CreateFunctionRequest;
  hasMetadata(): boolean;
  clearMetadata(): CreateFunctionRequest;

  getBackend(): BackendSpec | undefined;
  setBackend(value?: BackendSpec): CreateFunctionRequest;
  hasBackend(): boolean;
  clearBackend(): CreateFunctionRequest;

  getFrontend(): FrontendSpec | undefined;
  setFrontend(value?: FrontendSpec): CreateFunctionRequest;
  hasFrontend(): boolean;
  clearFrontend(): CreateFunctionRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateFunctionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateFunctionRequest): CreateFunctionRequest.AsObject;
  static serializeBinaryToWriter(message: CreateFunctionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateFunctionRequest;
  static deserializeBinaryFromReader(message: CreateFunctionRequest, reader: jspb.BinaryReader): CreateFunctionRequest;
}

export namespace CreateFunctionRequest {
  export type AsObject = {
    name: string,
    group: string,
    owner?: Account.AsObject,
    metadata?: Metadata.AsObject,
    backend?: BackendSpec.AsObject,
    frontend?: FrontendSpec.AsObject,
  }
}

export class CreateFunctionResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateFunctionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreateFunctionResponse): CreateFunctionResponse.AsObject;
  static serializeBinaryToWriter(message: CreateFunctionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateFunctionResponse;
  static deserializeBinaryFromReader(message: CreateFunctionResponse, reader: jspb.BinaryReader): CreateFunctionResponse;
}

export namespace CreateFunctionResponse {
  export type AsObject = {
  }
}

