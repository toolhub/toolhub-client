/**
 * @fileoverview gRPC-Web generated client stub for 
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck


import * as grpcWeb from 'grpc-web';

import * as protocol_function_service_pb from '../protocol/function_service_pb';


export class FunctionServiceClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: any; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'binary';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodInfoListFunctions = new grpcWeb.AbstractClientBase.MethodInfo(
    protocol_function_service_pb.ListFunctionResponse,
    (request: protocol_function_service_pb.ListFunctionRequest) => {
      return request.serializeBinary();
    },
    protocol_function_service_pb.ListFunctionResponse.deserializeBinary
  );

  listFunctions(
    request: protocol_function_service_pb.ListFunctionRequest,
    metadata: grpcWeb.Metadata | null): Promise<protocol_function_service_pb.ListFunctionResponse>;

  listFunctions(
    request: protocol_function_service_pb.ListFunctionRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: protocol_function_service_pb.ListFunctionResponse) => void): grpcWeb.ClientReadableStream<protocol_function_service_pb.ListFunctionResponse>;

  listFunctions(
    request: protocol_function_service_pb.ListFunctionRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: protocol_function_service_pb.ListFunctionResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/FunctionService/ListFunctions',
        request,
        metadata || {},
        this.methodInfoListFunctions,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/FunctionService/ListFunctions',
    request,
    metadata || {},
    this.methodInfoListFunctions);
  }

  methodInfoGetFunction = new grpcWeb.AbstractClientBase.MethodInfo(
    protocol_function_service_pb.GetFunctionResponse,
    (request: protocol_function_service_pb.GetFunctionRequest) => {
      return request.serializeBinary();
    },
    protocol_function_service_pb.GetFunctionResponse.deserializeBinary
  );

  getFunction(
    request: protocol_function_service_pb.GetFunctionRequest,
    metadata: grpcWeb.Metadata | null): Promise<protocol_function_service_pb.GetFunctionResponse>;

  getFunction(
    request: protocol_function_service_pb.GetFunctionRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: protocol_function_service_pb.GetFunctionResponse) => void): grpcWeb.ClientReadableStream<protocol_function_service_pb.GetFunctionResponse>;

  getFunction(
    request: protocol_function_service_pb.GetFunctionRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: protocol_function_service_pb.GetFunctionResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/FunctionService/GetFunction',
        request,
        metadata || {},
        this.methodInfoGetFunction,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/FunctionService/GetFunction',
    request,
    metadata || {},
    this.methodInfoGetFunction);
  }

  methodInfoCreateFunction = new grpcWeb.AbstractClientBase.MethodInfo(
    protocol_function_service_pb.CreateFunctionResponse,
    (request: protocol_function_service_pb.CreateFunctionRequest) => {
      return request.serializeBinary();
    },
    protocol_function_service_pb.CreateFunctionResponse.deserializeBinary
  );

  createFunction(
    request: protocol_function_service_pb.CreateFunctionRequest,
    metadata: grpcWeb.Metadata | null): Promise<protocol_function_service_pb.CreateFunctionResponse>;

  createFunction(
    request: protocol_function_service_pb.CreateFunctionRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: protocol_function_service_pb.CreateFunctionResponse) => void): grpcWeb.ClientReadableStream<protocol_function_service_pb.CreateFunctionResponse>;

  createFunction(
    request: protocol_function_service_pb.CreateFunctionRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: protocol_function_service_pb.CreateFunctionResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/FunctionService/CreateFunction',
        request,
        metadata || {},
        this.methodInfoCreateFunction,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/FunctionService/CreateFunction',
    request,
    metadata || {},
    this.methodInfoCreateFunction);
  }

  methodInfoListEndpoints = new grpcWeb.AbstractClientBase.MethodInfo(
    protocol_function_service_pb.ListEndpointResponse,
    (request: protocol_function_service_pb.ListEndpointRequest) => {
      return request.serializeBinary();
    },
    protocol_function_service_pb.ListEndpointResponse.deserializeBinary
  );

  listEndpoints(
    request: protocol_function_service_pb.ListEndpointRequest,
    metadata: grpcWeb.Metadata | null): Promise<protocol_function_service_pb.ListEndpointResponse>;

  listEndpoints(
    request: protocol_function_service_pb.ListEndpointRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: protocol_function_service_pb.ListEndpointResponse) => void): grpcWeb.ClientReadableStream<protocol_function_service_pb.ListEndpointResponse>;

  listEndpoints(
    request: protocol_function_service_pb.ListEndpointRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: protocol_function_service_pb.ListEndpointResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/FunctionService/ListEndpoints',
        request,
        metadata || {},
        this.methodInfoListEndpoints,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/FunctionService/ListEndpoints',
    request,
    metadata || {},
    this.methodInfoListEndpoints);
  }

}

