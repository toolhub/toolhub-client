import {FunctionServiceClient} from "./protocol/Function_serviceServiceClientPb";
import {AccessEndpoint, ListEndpointRequest, RPCEndpoint} from "./protocol/function_service_pb";

export {FunctionServiceClient} from "./protocol/Function_serviceServiceClientPb";
export {
	ListEndpointRequest, ListEndpointResponse,
	GetFunctionRequest, GetFunctionResponse,
	ListFunctionRequest, ListFunctionResponse,
	AccessEndpoint, RPCEndpoint, Function, Status,
	Metadata
} from "./protocol/function_service_pb";

export interface Query {
	match(address: string, accessPoint: AccessEndpoint): boolean;
};

export enum EndpointType {
	GRPC, HTTP, Gateway,
};

function matchAddress(query: string, hostname: string, endpoint: AccessEndpoint, rpc: RPCEndpoint, type: EndpointType): boolean {
	switch (type) {
		case EndpointType.GRPC:
			return query == rpc.getGrpc();
		
		case EndpointType.HTTP:
			return query == Endpoint.createHTTP(hostname, rpc.getHttp(), endpoint.getInsecure());
		
		case EndpointType.Gateway:
			return query == Endpoint.createHTTP(hostname, rpc.getGateway(), endpoint.getInsecure());

		default:
			return false;
	}
}

export class EndpointResult {
	hostname: string;
	endpoint: AccessEndpoint;

	constructor(hostname: string, endpoint: AccessEndpoint) {
		this.hostname = hostname;
		this.endpoint = endpoint;
	}
};

export class Endpoint {

	/**
	 * createHTTP creates an HTTP URL based on the given parameters.
	 * @param {string} hostname is the root hostname that can be used for relative paths.
	 * @param {string} location the descriptor location that may be relative or absolute.
	 * @param {boolean} insecure indicates whether the endpoints uses SSL.
	 */
	static createHTTP(hostname: string, location: string, insecure: boolean): string {
		let scheme = 'https';
		if (insecure)
			scheme = 'http';
		
		if (hostname == '*')
			hostname = '';

		if (location.startsWith('/'))
			return new URL(`${scheme}://${hostname}${location}`).toString();
		else
			return new URL(`${scheme}://${location}`).toString();
	}

	/**
	 * matchAPI returns a Query that finds matches using the API.
	 */
	static matchAPI(address: string, type: EndpointType): Query {
		let query: string = "";
		if (address.startsWith("http")) {
			query = address;
		} else {
			query = Endpoint.createHTTP(window.location.host, address, window.location.protocol != 'https');
		}
		return {
			match(hostname: string, e: AccessEndpoint): boolean {
				return matchAddress(query, hostname, e, e.getApi(), type);
			}
		}
	}


	/**
	 * select tried to find a match in an access endpoint collection.
	 */
	static select(endpoints: Map<string, AccessEndpoint>, query: Query): EndpointResult {
		let result: EndpointResult = null;
		endpoints.forEach((endpoint: AccessEndpoint, hostname: string) => {
			if (result != null)
				return;
			if (query.match(hostname, endpoint)) {
				result = new EndpointResult(hostname, endpoint);
			}
		});
		if (result != null)
			return result;
		if (endpoints.has("*")) {
			return new EndpointResult("", endpoints.get("*"));
		}
		return null;
	}
}

/**
 * FunctionManager provides helper methods to communicate and manager functions rendered on an iFrame.
 */
export class FunctionManager {

	client: FunctionServiceClient;
	endpoints: Map<string, AccessEndpoint>;
	target: EndpointResult;

	async connect(url: string) {
		this.client = new FunctionServiceClient(url);
		const result = await this.client.listEndpoints(new ListEndpointRequest(), {});
		this.endpoints = result.getEndpointsMap();
		this.target = Endpoint.select(this.endpoints, Endpoint.matchAPI(url, EndpointType.Gateway));
	}

	notifyURL(name: string, frame: HTMLIFrameElement): void {
		const message = {
			http: Endpoint.createHTTP(this.target.hostname, this.target.endpoint.getDispatcher().getHttp(), this.target.endpoint.getInsecure()) + `${name}/`,
		};
		frame.contentWindow.postMessage(message, '*');
	}

	install(name: string, frame: HTMLIFrameElement): void {
		frame.onload = () => {
			this.notifyURL(name, frame);
		};
		frame.src = Endpoint.createHTTP(this.target.hostname, this.target.endpoint.getCds(), this.target.endpoint.getInsecure()) + `${name}/`;
	}
};
